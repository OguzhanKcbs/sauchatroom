﻿using System.Web;
using System.Web.Optimization;

namespace ChatRoom
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/jquery-1.11.1.js",
                      "~/Scripts/menuScript.js",
                      "~/Scripts/jquery-latest.min.js",
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                       "~/Content/font-awesome.css",
                      "~/Content/style.css",
                      "~/Content/menuStyles.css",
                      "~/Content/footer.css"));

            bundles.Add(new StyleBundle("~/Content/Login_css").Include(
                    "~/Content/bootstrap.min.css",
                     "~/Content/Login_css/font-awesome.min.css",
                    "~/Content/Login_css/form-elements.css",
                    "~/Content/Login_css/style.css"));

            bundles.Add(new ScriptBundle("~/bundles/Login_js").Include(
                      "~/Scripts/jquery-1.11.1.js",
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/Login_js/jquery.backstretch.min.js",
                      "~/Scripts/Login_js/scripts.js"
                      ));
        }
    }
}

